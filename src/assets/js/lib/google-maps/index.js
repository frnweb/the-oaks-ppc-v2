import $ from 'jquery'
import googleMapsLoader from 'load-google-maps-api'
//

export const apiKey = 'AIzaSyAmjOz9vdCVJTyNqZMKTjTVelFH30oD8f0'

export const defaultMinHeight = 250
export const defaultHeight = '100%'

const ensureElementHasHeight = (
  $el,
  minHeight = defaultMinHeight,
  height = defaultHeight
) => {
  if ($el.height() <= 0) {
    $el.css({ minHeight, height })
  }
}

const readOptionsFromData = $el => {
  const lat = +$el.data('map-lat')
  const lng = +$el.data('map-lng')
  const zoom = +$el.data('map-zoom')

  if (!lat) {
    throw new Error('Please add a "data-map-lat" attribute with the latitude.')
  } else if (!lng) {
    throw new Error('Please add a "data-map-lng" attribute with the longitude.')
  } else if (!zoom) {
    throw new Error(
      'Please add a "data-map-zoom" attribute with the map zoom level.'
    )
  }

  return {
    center: {
      lat,
      lng,
    },
    zoom,
  }
}

export const googleMaps = stylePredicate => {
  $(document).ready(() => {
    const loader = googleMapsLoader({ key: apiKey })

    $('[data-map]').each((index, el) => {
      const $el = $(el)

      let styles

      if (typeof stylePredicate === 'function') {
        styles = stylePredicate($el.data('map-style'))
      }

      ensureElementHasHeight($el)

      const options = { ...readOptionsFromData($el), ...{ styles } }

      $el.each((index, el) => {
        const $el = $(el)

        loader.then(google => {
          const map = new google.Map(el, options)

          const markerOptions = {
            position: options.center,
            map,
            icon: $el.data('map-marker-url') || null,
            animation: google.Animation.DROP,
          }

          const marker = new google.Marker({ position: options.center, map, icon: "./assets/img/marker_sm.png" })

          const infoWindow = new google.InfoWindow({
            content: `
<div class="map--info">
  <h3>
    The Oaks at Lakeside
  </h3>
  <p>
    2911 Brunswick Road</br>Memphis, TN 38133
  </p>
  <a href="https://www.google.com/maps/dir/''/Lakeside+Behavioral+Health+System,+Brunswick+Road,+Memphis,+TN/@35.2067621,-89.8062743,13z/data=!4m8!4m7!1m0!1m5!1m1!1s0x887f9e06f0068669:0xcbd9f5f52d283da7!2m2!1d-89.771255!2d35.206697" target="_blank" class="button primary">Get Directions</a>
</div>
  `,
          })

          marker.addListener('click', () => {
            infoWindow.open(map, marker)
          })
        })
      })
    })
  })
}

export default googleMaps
