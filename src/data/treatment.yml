intro:
  id: treatment-intro
  heading: Accredited, Exceptional Care
  text: |
    At The Oaks, we’re ready when you’re ready: if you know it’s time to make a change, get control of your life and take the first major step on the road to recovery, we’re here for you. We offer a wide range of traditional and holistic approaches to treatment to give you the best possible chance of successful recovery from alcohol, substance use and co-occurring mental health disorders. Research tells us a multi-modal approach works best, so that’s what we offer: an array of options to meet your specific needs and goals.
  img: 'assets/img/duos/accredited-exceptional.jpg'
programs:
  id: programs
  heading: What We Treat
  items:
    -
      title: Alcohol Use Disorders
      content: |
        Our integrated approach to treating alcohol use disorders includes detoxification, individual therapy, group therapy, holistic therapies, and proactive aftercare planning. We take a different approach to alcohol addiction than most treatment centers: while many programs address addiction alone and ignore the co-occurring disorders common to most individuals with problematic alcohol use, we treat addiction alongside any other emotional and emotional challenges you face. The latest research proves this approach is more effective than treating addiction and mental health disorders in isolation. We treat the whole person, which dramatically increases your chance of long-term sobriety and recovery.
      img: 'assets/img/treatment/girl-glasses-smiling.jpg'

    -
      title: Substance Use Disorders
      content: |
        Our skilled, highly trained staff helps you make significant lifestyles changes, manage difficult emotions and develop the coping skills and stress-management tools necessary for successful, sustained abstinence. We collaborate with you to devise effective strategies for relapse prevention – strategies you start practicing before you leave treatment. Through individual therapy, group therapy and role-playing, we teach you how to recognize your personal warning signs, manage the triggers in your life, and reduce your risk of relapse. Our integrated model treats the whole person – including any dual-diagnoses or co-occurring disorders – which makes you more likely to sustain abstinence after you leave treatment.
      img: 'assets/img/treatment/father-daughter.jpg'

    -
      title: Mental Health Treatment and Co-Occurring Disorders 
      content: |
        At The Oaks we treat mental health disorders for patients both with and without a current substance abuse issue. Research shows that more than half the people struggling with alcohol and substance use disorders also have at least one additional mental health disorder. The same is true for individuals struggling with a primary mental health diagnosis: a majority also battle alcohol or substance use disorders. Mental health professionals use a variety of terms to describe this phenomenon: depending on the practitioner and treatment center, you’ll see or hear it called co-occurring disorders, dual diagnosis, co-morbidity, concurrent disorders or dual disorder diagnosis. Treatment at The Oaks addresses both chemical dependency and psychiatric illness with a comprehensive, integrated program that increases your likelihood of recovery from both.
      img: 'assets/img/treatment/three-female-friends.jpg'

    -
      title: Integrated Treatment
      content: |
        Our groundbreaking integrated treatment model is nationally recognized as the gold-standard template for treating alcohol, substance use and mental health disorders. The techniques developed here at The Oaks are considered best practices in addressing the co-occurring psychological and emotional issues that frequently accompany addiction. A long-range study conducted by leading mental health scientists at SAMHSA (The Substance Abuse and Mental Health Services Administration) determined The Foundations Treatment Model to be far more effective than traditional, single-issue treatment methods. We’re leaders in using creative techniques to help people achieve sobriety and experience sustained relief from the symptoms of emotional and psychological disorders.
      img: 'assets/img/treatment/older-family.jpg'

    -
      title: Medically Supervised Detox
      content: |
        A proper detoxification is the first and often the most important step in the recovery process. Detox may sound like a scary concept for those struggling with active addiction. We understand: we see it firsthand every day, and many of our staff have been through the detox process themselves. This makes them uniquely qualified to guide you through our medically supervised detox program as safely and comfortably as possible. Detox takes place in a separate, dedicated wing of our facility, with fully licensed and qualified medical staff monitoring your progress every step of the way.
      img: 'assets/img/treatment/girl-smiling.jpg'

    -
      title: Mental Health Assessments
      content: |
        Every patient at The Oaks starts with a complete mental health evaluation performed by licensed and credentialed professionals. We spend time getting to know you: we listen, we ask questions, and get a feel for who you are and what makes you tick. When we have an idea what will work for you – based on your input and our decades of experience – we design an integrated plan that leverages your strengths and shores up your challenge areas. We combine the best of traditional and complementary modes of therapy to ensure you start your recovery journey on solid ground.
      img: 'assets/img/treatment/family-sitting-laughing.jpg'
commitment:
  id: treatment-commitment
  heading: Our Commitment to You
  text: |
    We’re committed to offering you the best possible combination of integrated, evidence-based treatment options for individuals struggling with addiction and co-occurring disorders.
 
    Our award-winning, innovative approach to dual diagnosis treatment sets the standard to which all other treatment programs aspire.
 
    We’re committed to creating a custom treatment program that works for you.
