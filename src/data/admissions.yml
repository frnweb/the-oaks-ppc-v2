intro:
  id: admissions-intro
  heading: We Are Here to Help You
  text: |
    Imagine being one phone call away from taking back control of your life. We certainly understand that it can be an intimidating call to make, but it’s the most important one you’ll ever make. When you’re ready to take that first step, know that we aren’t here to judge you. In fact, many of our admissions coordinators are in recovery themselves, so they understand exactly what you’re going through. Regardless of who takes your call, we can guarantee you’ll be greeted by a compassionate and experienced member of our team dedicated to helping you through each step of the admissions process. Your new beginning really is just one phone call away.
  button-heading: All conversations are 100% confidential
  button-cta: |
    Get started now
  img: 'assets/img/duos/here-to-help.jpg'
process:
  id: admissions-process
  heading: What Is the Admissions Process?
  text: |
    The first step in the admissions process is to do an assessment to determine the appropriate level of care for you or your loved one. We guarantee that any information you share is 100% confidential. We just need to find out more about your specific situation so that we can match you up with the treatment that best meets your needs. Even if we aren’t the best fit for you, we’ll help get you pointed in the right direction.
assessment:
  id: assessment
  text: |
    **During the assessment, you can expect us to ask the following questions:**
    * What prompted you to reach out today?
    * Have you or your loved one been in treatment before?
    * In addition to alcohol and/or substance use disorders, do you or your loved one have any co-occurring mental health disorders that you know of?
    * Do you or your loved one have insurance coverage?
what-to-expect:
  id: what-to-expect
  text: |
    Once we perform our initial assessment and determine the appropriate level of care, we’ll schedule an admission as soon as possible. You can start treatment any day of the week, and we’re here year-round, ready to help you.
 
    We welcome patients from all over the country and our admissions coordinators can even help find the best options for you to get here, whether by car, plane or bus. If travel is required, we’ll do the legwork and you just purchase the tickets.
 
    Upon arrival at The Oaks, a residential coordinator will be available to answer any remaining questions you may have. You will complete some paperwork, receive a comprehensive medical assessment and take a tour of the facility. When all that’s finished, you join a warm, friendly community of peers and professionals dedicated to your recovery.
what-to-bring:
  id: what-to-bring
  heading: What Can You Bring to Treatment?
  text: |
    It’s helpful to be prepared for your stay in rehab. There are certain items that you may need to bring and others that are not allowed in our treatment facility.  A lot of thought goes into why we allow certain items and not others.  Our goal is for you to be totally focused on your health and well-being.
  items: 
    -
      title: Prepare what you will bring
      img: 'assets/img/duos/what-to-bring.jpg'
      content: |
        * Everyday clothes appropriate to the season. We have four seasons in Memphis: summers can be hot, winters can be cold, and in the Spring and Fall, we may have all four season in one afternoon.
        * Personal hygiene items
        * Approved prescription drugs: bring a minimum two-week supply in appropriately labeled bottles. The label should include your name, medication name, dosage, and frequency. Get these prescriptions filled before arrival. Our medical staff facilitates and monitors new or ongoing prescription needs.
        * Over-the-counter medications must be physician-approved, and will be maintained and dispensed by our staff.
        * Picture ID
        * Pharmacy Card or means to cover ongoing medication costs
    -
      title: What Items Are Commonly Not Allowed
      img: 'assets/img/duos/not-allowed.jpg'
      content: |
        * Revealing clothing: no short-shorts, midriff tops, spaghetti strap tops, short skirts, or tank tops with tiny straps
        * Clothes with images or language referring to alcohol, drugs, sex, or gang/criminal themes
        * Weapons, including pocket knives
        * Alcohol or drugs
        * Aerosol cans
        * Any product containing alcohol such as perfumes or mouthwash
        * Vitamins and supplements
        * Automobile
        * Personal electronics:
          - Laptops
          - Digital music devices
          - Video games
          - PDAs
          - Cameras
          - TVs
        * Food, including snacks, energy bars, or health food supplements
contact-us:
  id: contact-us
  heading: We Are Ready to Take Your Call
  text: |
    At The Oaks we strive to make the admissions process as quick and friendly as possible. Again, we understand that rehab admissions can feel intimidating; therefore, we do everything we can to show our patients compassion through every step.  Please call our 24-hour, toll-free helpline to begin your journey now.
  img: 'assets/img/theoaks-entrance.jpg'